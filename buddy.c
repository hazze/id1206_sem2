#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <assert.h>
#include <strings.h>
#include <time.h>


#define MIN 5
#define LEVELS 8
#define PAGE 4096
#define MAX_NUMBER_OF_PAGES 5


enum flag {Free, Taken};

int activePages = 0;

struct head {
  enum flag status;
  short int level;
  struct head *next;
  struct head *prev;
};

struct head *flists[LEVELS] = {NULL};

struct head *new() {
  struct head *new = (struct head *) mmap(NULL,
                                          PAGE,
                                          PROT_READ | PROT_WRITE,
                                          MAP_PRIVATE | MAP_ANONYMOUS,
                                          -1,
                                          0);
  if (new == MAP_FAILED) {
    return NULL;
  }
  assert(((long int) new & 0xfff) == 0); // 12 last bits should be zero
  new->status = Free;
  new->level = LEVELS - 1;

  activePages++;
  return new;
}

void add(struct head *block) {
  block->status = Free;
  if (flists[block->level] == NULL) {
    block->next = NULL;
    block->prev = NULL;
    flists[block->level] = block;
  }
  else {
    block->next = flists[block->level];
    block->next->prev = block;
    flists[block->level] = block;
  }
}

struct head *rem(int level) {
  struct head *block;
  if (flists[level] == NULL) {
    return NULL;
  }
  else if (flists[level]->next != NULL) {
    block = flists[level];
    flists[level]->next->prev = NULL;
    flists[level] = flists[level]->next;
  }
  else {
    block = flists[level];
    flists[level] = NULL;
  }
  return block;
}

void remSpec(struct head *block) {
  // MITTEN
  if (block->next != NULL && block->prev != NULL) {
    block->prev->next = block->next;
    block->next->prev = block->prev;
  }
  // FÖRST
  else if (block->next != NULL && block->prev == NULL) {
    flists[block->level] = block->next;
    block->next->prev = NULL;
  }
  // SIST
  else if (block->prev != NULL && block->next == NULL) {
    block->prev->next = NULL;
  }
  // ENSAM
  else {
    flists[block->level] = NULL;
  }
  block->next = NULL;
  block->prev = NULL;
}

struct head *buddy(struct head * block) {
  int index = block->level;
  long int mask = 0x1 << (index + MIN);
  return (struct head*)((long int) block ^ mask);
}

struct head *split(struct head *block) {
  int level = block->level - 1;
  int mask = 0x1 << (level + MIN);
  struct head *split = (struct head *)((long int) block | mask);
  split->level = level;
  block->level = level;
  return split;
}

struct head *primary(struct head * block) {
  int index = block->level;
  long int mask = 0xffffffffffffffff << (1 + index + MIN);
  struct head *primary = (struct head *) ((long int) block & mask);
  primary->level = index + 1;
  return primary;
}

void *hide(struct head* block) {
  return (void *)(block + 1);
}

struct head *magic(void *memory) {
  return ((struct head*) memory - 1);
}

int level(int req) {
  int total = req + sizeof(struct head);

  int i = 0;
  int size = 1 << MIN;
  while (total > size) {
    size <<= 1;
    i += 1;
  }
  return i;
}

struct head *find(int index, int level) {
  if (flists[index] == NULL) {
    if (index == LEVELS - 1) {
      add(new());
      find(index, level);
    }
    else {
      find(index + 1, level);
    }
  }
  else if (index != level) {
    struct head *block = split(rem(index));
    add(block);
    add(buddy(block));
    find(index - 1, level);
  }
  else {
    struct head *block = rem(index);
    block->level = index;
    return block;
  }
}

void *balloc(size_t size) {
  if (size == 0) {
    return NULL;
  }
  int index = level(size);
  struct head *taken = find(index, index);

  taken->status = Taken;
  return hide(taken);
}

void insert(struct head * block) {
  block->status = Free;
  if (block->level == LEVELS - 1) {
    if(activePages <= MAX_NUMBER_OF_PAGES) {
      add(block);
    }
    else {
      munmap(block, 4096);
      activePages--;
    }
  }
  else {
    struct head *bud = buddy(block);
    if ((bud->status == Free) && bud->level == block->level) {
      remSpec(bud);
      insert(primary(block));
    }
    else {
      add(block);
    }
    return;
  }
}

void bfree(void *memory) {
  if (memory != NULL) {
    struct head *block = magic(memory);
    insert(block);
  }
  return;
}

long get_size(void *memory) {
  struct head *block = magic(memory);
  switch (block->level) {
    case 0:
      return 8+24;
    case 1:
      return 40+24;
    case 2:
      return 104+24;
    case 3:
      return 232+24;
    case 4:
      return 488+24;
    case 5:
      return 1000+24;
    case 6:
      return 2024+24;
    case 7:
      return 4072+24;
  }
}

int get_page_count() {
  return activePages;
}

void print_flists() {
  for (int i = LEVELS - 1; i >= 0; i--) {
    printf("flists[%d]:\n", i);
    if (flists[i] != NULL) {
      printf("status -> %d\n", flists[i]->status);
      printf("& -> %p\n", flists[i]);
      printf("level -> %d\n", flists[i]->level);
      printf("next -> %p\n", flists[i]->next);
      printf("prev -> %p\n", flists[i]->prev);
      struct head *block = flists[i]->next;
      while (block != NULL) {
        printf("%4sflists[%d]->next\n", "", i);
        printf("%4sstatus -> %d\n", "", block->status);
        printf("%4s& -> %p\n", "", block);
        printf("%4slevel -> %d\n", "", block->level);
        printf("%4snext -> %p\n", "", block->next);
        printf("%4sprev -> %p\n", "", block->prev);
        block = block->next;
      }
      block = flists[i]->prev;
      while (block != NULL) {
        printf("%6sflists[%d]->prev\n", "", i);
        printf("%6sstatus -> %d\n", "", block->status);
        printf("%6s& -> %p\n", "", block);
        printf("%6slevel -> %d\n", "", block->level);
        printf("%6snext -> %p\n", "", block->next);
        printf("%6sprev -> %p\n", "", block->prev);
        block = block->prev;
      }
    }
    else {
      printf("NULL\n");
    }
    printf("\n");
  }
  printf("\n");
}

struct head *test_balloc(int size, char name) {
  struct head* temp = balloc(size);
  printf("%c->status: %d\n", name, temp->status);
  printf("&%c: %p\n", name, temp);
  printf("%c->level: %d\n", name, temp->level);
  printf("%c->next: %p\n", name, temp->next);
  printf("%c->prev: %p\n", name, temp->prev);
  printf("\n");
  return temp;
}

void test_constant(int count, int size) {
  clock_t start, middle, end;
  double balloc_time, bfree_time, totalt_time;
  double malloc_time, free_time, mtotalt_time;
  int *memory[count];
  int i;

  if(size == 0 || (size - 24) > 4096) {
    printf("incorrect size\n\n");
    return;
  }
  printf("Count: %d, Size: %d\n", count, size);
  // BALLOC BFREE
  start = clock();
  for (i = 0; i < count; i++) {
    memory[i] = balloc(size - 24);
  }
  middle = clock();
  for (i = 0; i < count; i++) {
    bfree(memory[i]);
  }
  end = clock();
  balloc_time = (((double) (middle - start)) / CLOCKS_PER_SEC) * 1000;
  bfree_time = (((double) (end - middle)) / CLOCKS_PER_SEC) * 1000;
  totalt_time = (((double) (end - start)) / CLOCKS_PER_SEC) * 1000;

  // MALLOC FREE
  start = clock();
  for (i = 0; i < count; i++) {
    memory[i] = malloc(size - 24);
  }
  middle = clock();
  for (i = 0; i < count; i++) {
    free(memory[i]);
  }
  end = clock();


  malloc_time = (((double) (middle - start)) / CLOCKS_PER_SEC) * 1000;
  free_time = (((double) (end - middle)) / CLOCKS_PER_SEC) * 1000;
  mtotalt_time = (((double) (end - start)) / CLOCKS_PER_SEC) * 1000;
  printf("balloc_time: %fms, average: %fms\n", balloc_time, balloc_time/count);
  printf("bfree_time: %fms, average: %fms\n", bfree_time, bfree_time/count);
  printf("totalt_time: %fms, average: %fms\n", totalt_time, totalt_time/count);
  printf("\n");
  printf("malloc_time: %fms, average: %fms\n", malloc_time, malloc_time/count);
  printf("free_time: %fms, average: %fms\n", free_time, free_time/count);
  printf("mtotalt_time: %fms, average: %fms\n", mtotalt_time, mtotalt_time/count);
  printf("\n");
}

void atest() {
  int *testArray[10000], *testArrayTwo[10000];
    for(int i = 0; i < 10000; i++){
        testArray[i] = balloc(sizeof(int));
    }
    for(int i = 0; i < 10000; i++){
        testArrayTwo[i] = balloc(sizeof(32));
    }
    for(int i = 0; i < 10000; i++){
        bfree(testArray[i]);
    }
    for(int i = 0; i < 10000; i++){
        bfree(testArrayTwo[i]);
    }
  // struct head* hiddenballoc = magic(balloc(8));
  // printf("hiddenballoc->status: %d\n", hiddenballoc->status);
  // printf("&hiddenballoc: %p\n", hiddenballoc);
  // printf("hiddenballoc->level: %d\n", hiddenballoc->level);
  // printf("hiddenballoc->next: %p\n", hiddenballoc->next);
  // printf("hiddenballoc->prev: %p\n", hiddenballoc->prev);
  // printf("\n");
  //
  // struct head* hiddenballocagain = magic(balloc(32));
  // printf("hiddenballocagain->status: %d\n", hiddenballocagain->status);
  // printf("&hiddenballocagain: %p\n", hiddenballocagain);
  // printf("hiddenballocagain->level: %d\n", hiddenballocagain->level);
  // printf("hiddenballocagain->next: %p\n", hiddenballocagain->next);
  // printf("hiddenballocagain->prev: %p\n", hiddenballocagain->prev);
  // printf("\n");
  // struct head *a = test_balloc(4, 'a');
  // struct head *b = test_balloc(4, 'b');
  // struct head *c = test_balloc(4, 'c');
  // struct head *d = test_balloc(4, 'd');
  // struct head *e = test_balloc(32, 'e');
  // struct head *b = test_balloc(256, 'b');
  // print_flists();
  // bfree(a);
  // bfree(c);
  // bfree(e);
  // bfree(d);
  // bfree(b);
  // print_flists();

  // struct head *mem = new();
  // printf("mem->status: %d\n", mem->status);
  // printf("&mem: %p\n", mem);
  // printf("mem->level: %d\n", mem->level);
  // printf("mem->next: %p\n", mem->next);
  // printf("mem->prev: %p\n", mem->prev);
  // printf("\n");
  //
  // struct head *splitmem = split(mem);
  // printf("splitmem->status: %d\n", splitmem->status);
  // printf("&splitmem: %p\n", splitmem);
  // printf("splitmem->level: %d\n", splitmem->level);
  // printf("splitmem->next: %p\n", splitmem->next);
  // printf("splitmem->prev: %p\n", splitmem->prev);
  // printf("\n");
  //
  // struct head *splitagain = split(splitmem);
  // printf("splitagain->status: %d\n", splitagain->status);
  // printf("&splitagain: %p\n", splitagain);
  // printf("splitagain->level: %d\n", splitagain->level);
  // printf("splitagain->next: %p\n", splitagain->next);
  // printf("splitagain->prev: %p\n", splitagain->prev);
  // printf("\n");
  //
  // struct head *hidehead = hide(splitagain);
  // printf("hidehead->status: %d\n", hidehead->status);
  // printf("&hidehead: %p\n", hidehead);
  // printf("hidehead->level: %d\n", hidehead->level);
  // printf("hidehead->next: %p\n", hidehead->next);
  // printf("hidehead->prev: %p\n", hidehead->prev);
  // printf("\n");
  //
  // struct head *findhead = magic(hidehead);
  // printf("findhead->status: %d\n", findhead->status);
  // printf("&findhead: %p\n", findhead);
  // printf("findhead->level: %d\n", findhead->level);
  // printf("findhead->next: %p\n", findhead->next);
  // printf("findhead->prev: %p\n", findhead->prev);
  // printf("\n");
  //
  // struct head *merge = primary(findhead);
  // printf("merge->status: %d\n", merge->status);
  // printf("&merge: %p\n", merge);
  // printf("merge->level: %d\n", merge->level);
  // printf("merge->next: %p\n", merge->next);
  // printf("merge->prev: %p\n", merge->prev);
  // printf("\n");
  //
  // struct head *mergeagain = primary(merge);
  // printf("mergeagain->status: %d\n", mergeagain->status);
  // printf("&mergeagain: %p\n", mergeagain);
  // printf("mergeagain->level: %d\n", mergeagain->level);
  // printf("mergeagain->next: %p\n", mergeagain->next);
  // printf("mergeagain->prev: %p\n", mergeagain->prev);
  // printf("\n");
}
