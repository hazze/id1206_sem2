#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include "rand.h"
#include "buddy.h"

#define BUFFER 100
#define SIZES 20000

int freq[SIZES];

int cmp(const void *a, const void *b) {
  return *(int*)a - *(int*)b;
}

int c[8] = {0};
void countsize(int size) {
  switch(size){
      case 32:
          c[0]++;
          break;
      case 64:
          c[1]++;
          break;
      case 128:
          c[2]++;
          break;
      case 256:
          c[3]++;
          break;
      case 512:
          c[4]++;
          break;
      case 1024:
          c[5]++;
          break;
      case 2048:
          c[6]++;
          break;
      case 4096:
          c[7]++;
          break;
  }
}

int main(int argc, char *argv[]) {

  if(argc < 3) {
    printf("usage: bench <rounds> <loop> <file>\n");
    exit(1);
  }

  int rounds = atoi(argv[1]);
  int loop = atoi(argv[2]);
  char *name = argv[3];

  void *init = sbrk(0);
  void *current;

  void *buffer[BUFFER];
  for(int i =0; i < BUFFER; i++) {
    buffer[i] = NULL;
  }

  long totalAlloc = 0;
  int maxSize = 0;
  int minSize= 4096;

  for(int j = 0; j < rounds; j++) {
    int alloc_round = 0;
    for(int i= 0; i < loop ; i++) {
      int index = rand() % BUFFER;
      if(buffer[index] != NULL) {
        totalAlloc -= get_size(buffer[index]);
        bfree(buffer[index]);
      }
      size_t size = (size_t)request();
      int *memory;
      memory = balloc(size);

      if(memory == NULL) {
        memory = balloc(0);
        fprintf(stderr, "memory myllocation failed, last address %p\n", memory);
        return(1);
      }
      buffer[index] = memory;
      totalAlloc += get_size(memory);
      countsize(get_size(memory));
    }
    alloc_round = totalAlloc/loop;
    int pages = get_page_count();

    if(j == 0) {
      FILE *file = fopen(name, "w");
      fprintf(file, "%d\t%d\t%ld\t%ld\t%d\t%ld\n", j+1, pages, totalAlloc/4096, totalAlloc, pages*4096, (pages*4096)-totalAlloc);
      fclose(file);
    }
    else {
      FILE *file = fopen(name, "a");
      fprintf(file, "%d\t%d\t%ld\t%ld\t%d\t%ld\n", j+1, pages, totalAlloc/4096, totalAlloc, pages*4096, (pages*4096)-totalAlloc);
      fclose(file);
    }
    current = sbrk(0);
    int allocated = (int)((current - init) / (1024));
  }
  FILE *sizefile = fopen("sizefile.dat", "w");
  fprintf(sizefile, "0\t32\t%d\n", c[0]);
  fprintf(sizefile, "1\t64\t%d\n", c[1]);
  fprintf(sizefile, "2\t128\t%d\n", c[2]);
  fprintf(sizefile, "3\t256\t%d\n", c[3]);
  fprintf(sizefile, "4\t512\t%d\n", c[4]);
  fprintf(sizefile, "5\t1024\t%d\n", c[5]);
  fprintf(sizefile, "6\t2048\t%d\n", c[6]);
  fprintf(sizefile, "7\t4096\t%d\n", c[7]);
  fclose(sizefile);

  return 0;
}
